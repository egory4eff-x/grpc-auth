package modules

import (
	"gitlab.com/egory4eff-x/grpc-auth/internal/infrastructure/component"
	aservice "gitlab.com/egory4eff-x/grpc-auth/internal/modules/auth/service"
	uservice "gitlab.com/egory4eff-x/grpc-auth/internal/modules/user/service"
	"gitlab.com/egory4eff-x/grpc-auth/internal/storages"
	"net/rpc"
)

type Services struct {
	User          uservice.Userer
	Auth          aservice.Auther
	UserClientRPC uservice.Userer
}

func NewServices(client *rpc.Client, storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserServiceJSONRPC(client)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
