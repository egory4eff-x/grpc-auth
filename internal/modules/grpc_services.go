package modules

import (
	"gitlab.com/egory4eff-x/grpc-auth/grpc/proto"
	"gitlab.com/egory4eff-x/grpc-auth/internal/infrastructure/component"
	aservice "gitlab.com/egory4eff-x/grpc-auth/internal/modules/auth/service"
	uservice "gitlab.com/egory4eff-x/grpc-auth/internal/modules/user/service"
	"gitlab.com/egory4eff-x/grpc-auth/internal/storages"
)

type GRPCServices struct {
	User          uservice.Userer
	Auth          aservice.Auther
	UserRPCClient uservice.Userer
}

func NewGRPCServices(client proto.UserServiceRPCClient, storages *storages.Storages, component *component.Components) *Services {
	userService := uservice.NewUserServiceGRPC(client)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, component),
	}
}
