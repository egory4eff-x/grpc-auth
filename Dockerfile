#FROM golang:1.19-alpine AS builder
#COPY . /go/src/gitlab.com/egory4eff-x/grpc-auth
#WORKDIR /go/src/gitlab.com/egory4eff-x/grpc-auth
#RUN go mod download && go build -ldflags="-w -s" -o /go/bin/server ./cmd/api/main.go
#
#FROM alpine:3.13
#COPY --from=builder /go/bin/server /go/bin/server
#COPY ./static/swagger.json /app/static/swagger.json
#COPY ./.env /app/.env
#
#WORKDIR /app
#ENTRYPOINT ["go/bin/server"]

#Build stage
FROM golang:1.19-alpine AS builder
WORKDIR /app
#Copy everything in the auth to the working directory in the docker
COPY ../grpc-auth .
#Build the file which has the name auth
RUN go build -o auth ./cmd/api/main.go

#Run stage
FROM alpine:3.13
WORKDIR /app
COPY --from=builder  /app/auth .
#Which port container listens to
#EXPOSE 8080
COPY static/swagger.json /app/static/swagger.json
COPY .env /app/.env
#The path to the executable file in the container
CMD ["/app/auth"]
