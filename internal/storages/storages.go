package storages

import (
	"gitlab.com/egory4eff-x/grpc-auth/internal/db/adapter"
	"gitlab.com/egory4eff-x/grpc-auth/internal/infrastructure/cache"
	vstorage "gitlab.com/egory4eff-x/grpc-auth/internal/modules/auth/storage"
)

type Storages struct {
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
