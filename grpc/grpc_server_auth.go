package grpcservice

import (
	"context"

	"gitlab.com/egory4eff-x/grpc-auth/grpc/proto"
	"gitlab.com/egory4eff-x/grpc-auth/internal/modules/auth/service"
)

type GRCPServerAuth struct {
	proto.UnimplementedAuthServiceRPCServer
	a service.Auther
}

func NewGRCPServerAuth(a service.Auther) *GRCPServerAuth {
	return &GRCPServerAuth{a: a}
}

func (g *GRCPServerAuth) Register(ctx context.Context, in *proto.RegisterIn) (*proto.RegisterOut, error) {
	out := g.a.Register(ctx, service.RegisterIn{
		Email:    in.GetEmail(),
		Password: in.GetPassword(),
	}, int(in.GetField()))
	return &proto.RegisterOut{
		Status:    int32(out.Status),
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (g *GRCPServerAuth) AuthorizeEmail(ctx context.Context, in *proto.AuthorizeEmailIn) (*proto.AuthorizeOut, error) {
	out := g.a.AuthorizeEmail(ctx, service.AuthorizeEmailIn{
		Email:          in.GetEmail(),
		Password:       in.GetPassword(),
		RetypePassword: in.GetRetypePassword(),
	})
	return &proto.AuthorizeOut{
		UserId:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}
func (g *GRCPServerAuth) AuthorizeRefresh(ctx context.Context, in *proto.AuthorizeRefreshIn) (*proto.AuthorizeOut, error) {
	out := g.a.AuthorizeRefresh(ctx, service.AuthorizeRefreshIn{
		UserID: int(in.GetUserId()),
	})

	return &proto.AuthorizeOut{
		UserId:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (g *GRCPServerAuth) AuthorizePhone(ctx context.Context, in *proto.AuthorizePhoneIn) (*proto.AuthorizeOut, error) {
	out := g.a.AuthorizePhone(ctx, service.AuthorizePhoneIn{
		Phone: in.GetPhone(),
		Code:  int(in.GetCode()),
	})
	return &proto.AuthorizeOut{
		UserId:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}
func (g *GRCPServerAuth) SendPhoneCode(ctx context.Context, in *proto.SendPhoneCodeIn) (*proto.SendPhoneCodeOut, error) {
	out := g.a.SendPhoneCode(ctx, service.SendPhoneCodeIn{Phone: in.GetPhone()})
	return &proto.SendPhoneCodeOut{
		Phone: out.Phone,
		Code:  int32(out.Code),
	}, nil

}
func (g *GRCPServerAuth) VerifyEmail(ctx context.Context, in *proto.VerifyEmailIn) (*proto.VerifyEmailOut, error) {
	out := g.a.VerifyEmail(ctx, service.VerifyEmailIn{
		Hash:  in.GetHash(),
		Email: in.GetEmail(),
	})
	return &proto.VerifyEmailOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}
